package swamphack.recordrunner;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PrefFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PrefFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PrefFragment extends android.support.v4.app.Fragment {


    public static PrefFragment newInstance() {
        PrefFragment fragment = new PrefFragment();
        return fragment;
    }

    public PrefFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_preference, container, false);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
