package swamphack.recordrunner;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;


public class MainActivity extends FragmentActivity {
    ArrayList<Fragment> fragList;
    CustomViewPager mViewPager;
    MyPagerAdapter adapterViewPager;

    private ArrayList<Fragment> setFragments(){
        ArrayList<Fragment> list = new ArrayList<Fragment>();
        PrefFragment pf = PrefFragment.newInstance();
        RunningFragment rf = RunningFragment.newInstance();
        list.add(pf);
        list.add(rf);
        this.fragList = list;
        return list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        //set up the swipe-able viewpager for the fragments
        mViewPager = (CustomViewPager)findViewById(R.id.viewpager);
        adapterViewPager  = new MyPagerAdapter(setFragments());
        mViewPager.setAdapter(adapterViewPager);
        mViewPager.setPagingEnabled(true);
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {}
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            public void onPageSelected(int position) {}
        });
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {
        private ArrayList<Fragment> fragments;
        Context context;

        //Constructor
        public MyPagerAdapter(ArrayList<Fragment> fragments) {
            super(getSupportFragmentManager());
            this.fragments = fragments;
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return fragments.size();
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
